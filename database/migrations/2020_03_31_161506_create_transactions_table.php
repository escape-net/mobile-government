<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('payment_reference')->nullable();
            $table->float('amount')->nullable();
            $table->string('status')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->datetime('paid_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
