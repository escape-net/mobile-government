<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversLicenseRenewalsTable extends Migration
{

    public function up()
    {
        Schema::create('drivers_license_renewals', function (Blueprint $table) {

            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('license_number')->nullable();
            $table->string('birth_date')->nullable();
            $table->integer('years')->nullable();
            $table->float('amount')->nullable();
            $table->enum('status', ['incomplete', 'cancelled', 'pending', 'paid', 'processed'])->default('incomplete')->nullable();
            $table->timestamps();
        });
    }

  
    public function down()
    {
        Schema::dropIfExists('drivers_license_renewals');
    }
}
