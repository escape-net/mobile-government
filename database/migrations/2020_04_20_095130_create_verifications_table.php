<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerificationsTable extends Migration
{
    public function up()
    {
        Schema::create('verifications', function (Blueprint $table) {

            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('bank_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->enum('type', ['bvn', 'nin', 'drivers_license', 'intl_passport', 'voters_card', 'cac', 'tin'])->nullable();
            $table->string('bvn')->nullable();
            $table->string('nin')->nullable();
            $table->string('drivers_license')->nullable();
            $table->string('intl_passport')->nullable();
            $table->string('voters_card')->nullable();
            $table->string('cac')->nullable();
            $table->string('tin')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->datetime('birth_date')->nullable();
            $table->string('rc_number')->nullable();
            $table->float('amount')->nullable();
            $table->text('response')->nullable();
            $table->enum('status', ['incomplete', 'cancelled', 'pending', 'paid', 'processed', 'failed'])->default('incomplete')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('verifications');
    }
}
