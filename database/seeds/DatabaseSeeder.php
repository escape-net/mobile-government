<?php

use Illuminate\Database\Seeder;
use App\Stage;
use App\VerificationService;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $stages = [

            [

                'text'          => null,
                'method'        => 'Initialize'
            ],

            [

                'text'          => '1',
                'method'        => 'Register'
            ],

            [

                'text'          => '2',
                'method'        => 'MakeVerifications'
            ],

            [

                'text'          => '3',
                'method'        => 'GeneratePaymentCode'
            ],

            [

                'text'          => '4',
                'method'        => 'MakeTSAPayment'
            ],

            [

                'text'          => '5',
                'method'        => 'MakeRenewals'
            ]
            
        ];


        foreach($stages as $row)
            Stage::create($row);

        $verification_services = [

            ['name' => 'Verify BVN', 'code' => '1', 'response' => 'Please enter a BVN'],
            ['name' => 'Verify Drivers License', 'code' => '2', 'response' => 'Please enter a Drivers License Number'],
            ['name' => 'Verify Passport', 'code' => '3', 'response' => 'Please enter a Passport Number'],
            ['name' => 'Verify Company', 'code' => '4', 'response' => 'Please enter a Company CAC Number'],
            ['name' => 'Verify TIN', 'code' => '5', 'response' => 'Please enter a TIN']
        ];

        foreach($verification_services as $row)
            VerificationService::create($row);
    }
}
