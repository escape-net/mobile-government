-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 02, 2020 at 10:24 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onepipe_loanrequest`
--

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sort_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `code`, `name`, `sort_code`) VALUES
(1, '044', 'ACCESS BANK PLC', '044150149'),
(2, '014', 'MAINSTREET BANK PLC', '014150030'),
(3, '082', 'KEYSTONE BANK PLC', '082150017'),
(4, '063', 'DIAMOND BANK PLC', '063150162'),
(5, '050', 'ECOBANK NIGERIA PLC', '050150311'),
(6, '040', 'EQUITORIAL TRUST BANK LIMITED', NULL),
(7, '070', 'FIDELITY BANK PLC', '070150003'),
(8, '011', 'FIRST BANK OF NIGERIA PLC', '011152303'),
(9, '214', 'FIRST CITY MONUMENT BANK PLC', '214150018'),
(10, '085', 'FIRST INLAND BANK PLC', NULL),
(11, '058', 'GUARANTY TRUST BANK PLC', '058152052'),
(12, '221', 'STANBIC IBTC BANK PLC', '221159522'),
(13, '069', 'INTERCONTINENTAL BANK PLC', NULL),
(14, '023', 'CITI BANK', NULL),
(15, '056', 'OCEANIC BANK INTERNATIONAL PLC', NULL),
(16, '076', 'SKYE BANK PLC', '076151006'),
(17, '084', 'ENTERPRISE BANK PLC', NULL),
(18, '068', 'STANDARD CHARTERED BANK PLC', '068150057'),
(19, '232', 'STERLING BANK PLC', '232150029'),
(20, '032', 'UNION BANK OF NIGERIA PLC', '032156825'),
(21, '033', 'UNITED BANK FOR AFRICA PLC', '033154282'),
(22, '215', 'UNITY BANK PLC', '215082334'),
(23, '035', 'WEMA BANK PLC', '035150103'),
(24, '057', 'ZENITH INTERNATIONAL BANK PLC', ' 057150013'),
(25, '301', 'JAIZ BANK', '301080020'),
(26, '001', 'CENTRAL BANK OF NIGERIA', NULL),
(27, '030', 'HERITAGE BANK', NULL),
(28, '559', 'Coronation Merchant Bank', NULL),
(29, '100', 'SunTrust ', NULL),
(30, '502', 'Parallex', NULL),
(31, '317', 'Cellulant', NULL),
(32, '608', 'Finatrust Microfinance Bank', NULL),
(33, '101', 'Providus Bank ', NULL),
(34, '327', 'Paga', NULL),
(35, '302', 'Eartholeum (Qik Qik)', NULL),
(36, '303', 'Kegow', NULL),
(37, '304', 'Stanbic Mobile', NULL),
(38, '305', 'Paycom', NULL),
(39, '306', 'E-Tranzact', NULL),
(40, '307', 'EcoMobile', NULL),
(41, '308', 'Fortis Mobile', NULL),
(42, '309', 'FBN M-money', NULL),
(43, '310', 'Corporetti', NULL),
(44, '311', 'Parkway (Ready Cash)', NULL),
(45, '312', 'Monetize', NULL),
(46, '313', 'M-Kudi', NULL),
(47, '314', 'Fets (My Wallet)', NULL),
(48, '315', 'GT Mobile Money', NULL),
(49, '316', 'U-MO', NULL),
(50, '401', 'ASO Savings And Loans', NULL),
(51, '319', 'Teasy Mobile', NULL),
(52, '320', 'VTNetwork', NULL),
(53, '323', 'AccessMobile', NULL),
(54, '324', 'Hedonmark', NULL),
(55, '322', 'Zenith Mobile', NULL),
(56, '501', 'Fortis Microfinance Bank', NULL),
(57, '318', 'Fidelity Mobile', NULL),
(58, '402', 'Jubilee Life', NULL),
(59, '325', 'Moneybox', NULL),
(60, '523', 'Trustbond', NULL),
(61, '601', 'FSDH', NULL),
(62, '551', 'Covenant Microfinance Bank', NULL),
(63, '552', 'NPF MFB', NULL),
(64, '326', 'Sterling Mobile', NULL),
(65, '328', 'VISUAL ICT', NULL),
(66, '403', 'SafeTrust Mortgage Bank', NULL),
(67, '329', 'PayAttitude Online', NULL),
(68, '606', 'Omoluabi Savings and Loans PLC', NULL),
(69, '415', 'IMPERIAL HOMES MORTGAGE BANK', NULL),
(70, '561', 'New Prudential Bank', NULL),
(71, '560', 'Page MFBank', NULL),
(72, '413', 'FBN Mortgages Limited', NULL),
(73, '565', 'ONE FINANCE', NULL),
(74, '330', 'miMONEY ( Powered by IntelliFin)', NULL),
(75, '566', 'VFD MICROFINANCE BANK ', NULL),
(76, '562', 'EKONDO MICROFINANCE BANK  ', NULL),
(77, '609', 'SEED CAPITAL MICROFINANCE BANK', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
