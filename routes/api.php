<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api'], function(){

	Route::any('/ussd/{step?}', 'UssdController@index');
	Route::any('/rave-callback', 'RaveCallbackController@index');

	Route::get('steps/{id?}', 'StepsController@index');
	Route::post('step/save', 'StepsController@save');
});