<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $guarded  = ['updated_at'];

    public static function steps()
    {
    	return [

    		['parent_id' => null, 'starts_with' => null, 'level' => null, 'class' => 'Initialize', 'method' => 'index'],
    		
    		['parent_id' => 1, 'starts_with' => 1, 'level' => 0, 'class' => 'Registration', 'method' => 'index'],
    		['parent_id' => 1, 'starts_with' => 1, 'level' => 1, 'class' => 'Registration', 'method' => 'name'],
    		['parent_id' => 1, 'starts_with' => 1, 'level' => 2, 'class' => 'Registration', 'method' => 'email'],

            ['parent_id' => 1, 'starts_with' => 2, 'level' => 0, 'class' => 'VerifyBVN', 'method' => 'index'],
            ['parent_id' => 1, 'starts_with' => 2, 'level' => 1, 'class' => 'VerifyBVN', 'method' => 'bvn'],
            ['parent_id' => 1, 'starts_with' => 2, 'level' => 2, 'class' => 'VerifyBVN', 'method' => 'bank'],
            ['parent_id' => 1, 'starts_with' => 2, 'level' => 3, 'class' => 'VerifyBVN', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 3, 'level' => 0, 'class' => 'VerifyNIN', 'method' => 'index'],
    		['parent_id' => 1, 'starts_with' => 3, 'level' => 1, 'class' => 'VerifyNIN', 'method' => 'nin'],
    		['parent_id' => 1, 'starts_with' => 3, 'level' => 2, 'class' => 'VerifyNIN', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 3, 'level' => 3, 'class' => 'VerifyNIN', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 4, 'level' => 0, 'class' => 'VerifyDriversLicense', 'method' => 'index'],
            ['parent_id' => 1, 'starts_with' => 4, 'level' => 1, 'class' => 'VerifyDriversLicense', 'method' => 'drivers_license'],
            ['parent_id' => 1, 'starts_with' => 4, 'level' => 2, 'class' => 'VerifyDriversLicense', 'method' => 'name'],
    		['parent_id' => 1, 'starts_with' => 4, 'level' => 3, 'class' => 'VerifyDriversLicense', 'method' => 'birth_date'],
    		['parent_id' => 1, 'starts_with' => 4, 'level' => 4, 'class' => 'VerifyDriversLicense', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 4, 'level' => 5, 'class' => 'VerifyDriversLicense', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 5, 'level' => 0, 'class' => 'VerifyIntlPassport', 'method' => 'index'],
            ['parent_id' => 1, 'starts_with' => 5, 'level' => 1, 'class' => 'VerifyIntlPassport', 'method' => 'passport_number'],
            ['parent_id' => 1, 'starts_with' => 5, 'level' => 2, 'class' => 'VerifyIntlPassport', 'method' => 'name'],
    		['parent_id' => 1, 'starts_with' => 5, 'level' => 3, 'class' => 'VerifyIntlPassport', 'method' => 'birth_date'],
    		['parent_id' => 1, 'starts_with' => 5, 'level' => 4, 'class' => 'VerifyIntlPassport', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 5, 'level' => 5, 'class' => 'VerifyIntlPassport', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 6, 'level' => 0, 'class' => 'VerifyVotersCard', 'method' => 'index'],
    		['parent_id' => 1, 'starts_with' => 6, 'level' => 1, 'class' => 'VerifyVotersCard', 'method' => 'voters_card'],
    		['parent_id' => 1, 'starts_with' => 6, 'level' => 2, 'class' => 'VerifyVotersCard', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 6, 'level' => 3, 'class' => 'VerifyVotersCard', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 7, 'level' => 0, 'class' => 'VerifyCAC', 'method' => 'index'],
            ['parent_id' => 1, 'starts_with' => 7, 'level' => 1, 'class' => 'VerifyCAC', 'method' => 'cac'],
    		['parent_id' => 1, 'starts_with' => 7, 'level' => 2, 'class' => 'VerifyCAC', 'method' => 'rc_number'],
    		['parent_id' => 1, 'starts_with' => 7, 'level' => 3, 'class' => 'VerifyCAC', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 7, 'level' => 4, 'class' => 'VerifyCAC', 'method' => 'sms'],

    		['parent_id' => 1, 'starts_with' => 8, 'level' => 0, 'class' => 'VerifyTIN', 'method' => 'index'],
    		['parent_id' => 1, 'starts_with' => 8, 'level' => 1, 'class' => 'VerifyTIN', 'method' => 'tin'],
    		['parent_id' => 1, 'starts_with' => 8, 'level' => 2, 'class' => 'VerifyTIN', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 8, 'level' => 3, 'class' => 'VerifyTIN', 'method' => 'sms'],
    		
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 0, 'class' => 'RenewDriversLicense', 'method' => 'index'],
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 1, 'class' => 'RenewDriversLicense', 'method' => 'birth_day'],
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 2, 'class' => 'RenewDriversLicense', 'method' => 'years'],
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 3, 'class' => 'RenewDriversLicense', 'method' => 'payment'],
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 4, 'class' => 'RenewDriversLicense', 'method' => 'bank'],
    		['parent_id' => 1, 'starts_with' => 9, 'level' => 5, 'class' => 'RenewDriversLicense', 'method' => 'sms'],
    	];
    }
}
