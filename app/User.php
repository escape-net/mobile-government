<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];
    protected $dates    = ['created_at', 'updated_at'];

    public function profile()
    {
    	return $this->hasOne('App\Profile');
    }

    public function drivers_license_renewals()
    {
    	return $this->hasMany('App\DriversLicenseRenewal');
    }
}
