<?php 

namespace App\Traits;

use App\User;
use App\Stage;
use Illuminate\Support\Str;

trait UssdHelpers {

	public function reply($reply, $continue = 'CON')
	{
		$reply = $continue.' '.$reply;

		return response($reply, 200)->header('Content-Type', 'text/plain');
	}

	public function getUser($phone = '', $network_code = '')
	{
		$phone = str_replace('+', '', $phone);

		return User::updateOrCreate(['phone' => $phone], [

			'phone'			=> $phone,
			'network_code'	=> $network_code
		]);
	}

	public function getUssdData($text)
	{
		if(!Str::contains($text, '*'))
			return collect([]);
		
		$data = substr($text, strpos($text, '*')+1, strlen($text));

		return collect(explode('*', $data));
	}
}