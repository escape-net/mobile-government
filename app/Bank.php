<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $hidden  = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function transactions()
    {
    	return $this->hasMany('App\Transaction');
    }

    public static function allowedBanks()
    {
    	return self::whereIn('id', [3, 7, 11, 19, 21, 22, 24])->get();
    }
}
