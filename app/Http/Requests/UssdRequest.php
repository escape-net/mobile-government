<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UssdRequest extends FormRequest
{
    public function rules()
    {
        return [
            
            'phoneNumber'   => 'required',
            'serviceCode'   => 'required',
            'text'          => 'required',
            'sessionId'     => 'required',
            'networkCode'   => 'required'
        ];
    }
}
