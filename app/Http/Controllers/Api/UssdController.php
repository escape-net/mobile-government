<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UssdHelpers;
use App\User;
use App\Step;
use Illuminate\Support\Str;

class UssdController extends Controller
{
    use UssdHelpers;

    public function index(Request $request, $return_step = null)
    {
        try{
            
            $user   = $this->getUser(request('phoneNumber'), request('networkCode'));
            $text   = request('text');
            $input  = $this->getUssdData($text);

            $step = null;

            if($text == null || strlen($text) == 0){

                $step = Step::whereNull('starts_with')->first();

            }else{

                $step = Step::where([

                    'starts_with'   => $text[0], 
                    'level'         => $input->count() 

                ])->first();
            }

            if($step == null)
                return $this->reply('Service unavaliable.');

            if($return_step){

                return ['step' => $step, 'input' => $input];
            }
            
            $class  = "App\Steps\\$step->class";

            return (new $class($user, $input))->{$step->method}();

        }catch(Exception $e){

            return $this->reply('An Error occured.', 'END');
        }

    }
}
