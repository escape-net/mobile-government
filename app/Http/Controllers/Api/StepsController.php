<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Step;
use App\Steps;

class StepsController extends Controller
{
    public function index($id = 0)
    {
        $data = $id > 0 ? Step::find($id) : Step::get();

        return response()->json([

          'status'  => true,
          'data'        => $data
      ]);
    }

    public function save(Request $request)
    {
        Step::query()->truncate();
        $steps  = Step::steps();
        $errors = [];

        foreach($steps as $row){

            $class      = $row['class'];
            $method     = $row['method'];

            $class_name = "App\Steps\\$class";

            if(!class_exists($class_name)){

                $errors[] = ['Class '.$row['class'].' does not exist'];
                continue;
            }

            $object = new $class_name(null, null);

            if(!method_exists($object, $method)){

                $errors[] = "Method ($method) does not exist in $class_name";
                continue;
            }

            Step::updateOrCreate(['class' => $class, 'method' => $method], [

                'parent_id'     => $row['parent_id'],
                'starts_with'   => $row['starts_with'],
                'level'         => $row['level'],
                'class'         => $class,
                'method'        => $method
            ]);

        }
        
        return response()->json([

            'status'    => true,
            'data'      => [

                'errors'    => $errors,
                'steps'     => Step::get()
            ]
        ]);
    }
}
