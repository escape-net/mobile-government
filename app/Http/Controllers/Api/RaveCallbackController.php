<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;

class RaveCallbackController extends Controller
{
    public function index(Request $request)
    {                       
        $this->validate($request, [

            'txRef'             => 'required',
            'flwRef'            => 'required',
            'amount'            => 'required',
            'charged_amount'    => 'required',
            'status'            => 'required'
        ]);

        $payment_reference      = request('txRef');
        $transaction_reference  = request('flwRef');
        $amount                 = request('amount');
        $charged_amount         = request('charged_amount');
        $status                 = request('status');

        if($status != 'successful'){

            return response()->json(['status' => false, 'message' => 'Payment not successful.']);
        }

        $transaction = Transaction::where([

            'payment_reference' => $payment_reference,
            'status'            => 'pending'

        ])->first();

        if(!$transaction){

            return response()->json(['status' => false, 'message' => 'Invalid Transaction']);
        }

        if($transaction->amount != $charged_amount){

            return response()->json(['status' => false, 'message' => 'Charged amount does not match transaction amount.']);
        }

        $transaction->update([

            'status'                => 'paid',
            'paid_at'               => date('Y-m-d H:i:s'),
            'transaction_reference' => request('flwRef')
        ]);

        Transaction::execute($transaction);

        return response()->json([

            'status'    => true, 
            'data'      => 'Payment updated succcessfully.'
        ]);

    }
}
