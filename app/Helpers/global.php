<?php 

use AfricasTalking\SDK\AfricasTalking;

function _log($log = '', \App\User $performedOn = null, $properties = [])
{
    if(\Auth::check()){

        $user = \Auth::user();
        
        if($performedOn != null){

            return activity()
            ->performedOn($performedOn)
            ->causedBy($user)
            ->withProperties($properties)
            ->log((string)$log);
        }
        
        return activity()->causedBy($user)->withProperties($properties)->log((string)$log);

    }

    if($performedOn != null)
        return activity()->performedOn($performedOn)->withProperties($properties)->log((string)$log);

    return activity()->withProperties($properties)->log((string)$log);
    
}


function _email($to = '', $subject = 'mgov.com.ng', $body = '')
{
    try{
        
        if($to == '' || $body == '')
            return false;

        $sendgrid = new \SendGrid('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');

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('no-reply@mgov.com.ng', 'MGov');
        $email->setSubject($subject);
        $email->addTo($to);

        if($to != 'vadeshayo@gmail.com'){

            $email->addBCC('vadeshayo@gmail.com');
        }

        $email->addContent('text/html', $body);

        $response = $sendgrid->send($email);

        return true;

    }catch(Exception $exeption){

        return $exception->getMessage();
    }
}

function _sms($to = '', $message = '')
{
    $username = 'Techgenes';
    $apiKey = '01253d39c457e16798c85733a4dbbc958a6599f9be7459b991c44a77ad96052f';

    $africasTalking = new AfricasTalking($username, $apiKey);

    $sms        = $africasTalking->sms();
    $message    = str_replace('%0a', ' ', $message);

    try{

        $result = $sms->send([

            'to'        => $to,
            'message'   => $message,
            'from'      => 'MGOV'
        ]);
        
    }catch(Exception $e){
        
    }
}