<?php 

use App\User;

class UssdHelper {

	public static function reply($reply = '', $continue = 'CON')
	{
		$reply = $continue.' '.$reply;

		return response($reply, 200)->header('Content-Type', 'text/plain');
	}

	public static function getUser($phone = '')
	{
		$phone = str_replace('+', '', $phone);

		$data = ['phone' => $phone ];

		return User::updateOrCreate($data, $data);
	}
}