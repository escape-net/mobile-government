<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Jobs\ExecuteDriversLicenseRenewal;
use App\Jobs\ExecuteBVNVerification;
use App\Jobs\ExecuteNINVerification;
use App\Jobs\ExecuteDriversLicenseVerification;
use App\Jobs\ExecuteVotersCardVerification;
use App\Jobs\ExecuteCACVerification;
use App\Jobs\ExecuteTINVerification;
use App\Jobs\ExecuteIntlPassportVerification;

class Transaction extends Model
{
    protected $hidden  = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

    		return new User();
    	});
    }

    public function bank()
    {
    	return $this->belongsTo('App\Bank')->withDefault(function(){

    		return new Bank();
    	});
    }

    public static function execute($transaction)
    {
        $drivers_license_renewal = DriversLicenseRenewal::whereTransactionId($transaction->id)->first();

        if($drivers_license_renewal){

            $drivers_license_renewal->update(['status' => 'paid']);
            return ExecuteDriversLicenseRenewal::dispatch($drivers_license_renewal);
        }

        $verification = Verification::whereTransactionId($transaction->id)->first();

        if(!$verification)
            return;

        $verification->update(['status' => 'paid']);
        
        if($verification->type == 'bvn')
            return ExecuteBVNVerification::dispatch($verification);

        if($verification->type == 'nin')
            return ExecuteNINVerification::dispatch($verification);

        if($verification->type == 'drivers_license')
            return ExecuteDriversLicenseVerification::dispatch($verification);

        if($verification->type == 'intl_passport')
            return ExecuteIntlPassportVerification::dispatch($verification);

        if($verification->type == 'nin')
            return ExecuteVotersCardVerification::dispatch($verification);

        if($verification->type == 'cac')
            return ExecuteCACVerification::dispatch($verification);

        if($verification->type == 'tin')
            return ExecuteTINVerification::dispatch($verification);
    }
    
}
