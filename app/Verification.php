<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $hidden  = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

    		return new User();
    	});
    }

    public function bank()
    {
    	return $this->belongsTo('App\Bank')->withDefault(function(){

    		return new Bank();
    	});
    }

    public function transaction()
    {
    	return $this->belongsTo('App\Transaction')->withDefault(function(){

    		return new Transaction();
    	});
    }
}
