<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteTINVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $response = OneGov::get('tin/verify?tin='.$this->verification->tin);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'TIN '.$this->verification->tin.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your TIN Number '.$this->verification->tin.' is valid. %0a';
            $sms .= 'Name: '.optional($info)->firstName.' '.optional($info)->lastName.' '.optional($info)->otherName.' %0a';
            $sms .= 'Date of Birth: '.optional($info)->dateOfBirth.' %0a';
            $sms .= 'Issued At: '.optional($info)->issueDate.' %0a';
            $sms .= 'ID Number: '.optional($info)->idNumber.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);         
    }
}
