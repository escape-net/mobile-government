<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Transaction;
use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;
use App\Services\SMS;

class RaveTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function handle()
    {
        try{

            $transaction = $this->transaction;
            $bank        = $transaction->bank;
            $user        = $transaction->user;

            $data = [

                'PBFPubKey'     => env('RAVE_PUBLIC_KEY'),
                'accountbank'   => $bank->code,
                'currency'      => 'NGN',
                'country'       => 'NG',
                'amount'        => $transaction->amount,
                'email'         => $user->email ?? 'vadeshayo@gmail.com',
                'phonenumber'   => $user->phone,
                'firstname'     => $user->first_name,
                'lastname'      => $user->last_name,
                'is_ussd'       => 1,
                'payment_type'  => 'USSD',
                'txRef'         => $transaction->payment_reference,
                'orderRef'      => $transaction->payment_reference
            ];

            $encrypted_data = $this->encrypt3Des(json_encode($data), $this->getKey());

            $payload = [

                'PBFPubKey' => env('RAVE_PUBLIC_KEY'),
                'client'    => $encrypted_data,
                'alg'       => '3DES-24'
            ];

            $response = (new Client)->post('https://api.ravepay.co/flwv3-pug/getpaidx/api/charge', [

                'headers' => [

                    'Accept' => 'application/json'
                ],

                RequestOptions::JSON => $payload
            ]);

            if((int)$response->getStatusCode() != 200){

                $response = $response->getBody()->getContents();

                $sms = 'We ran into some issues while initiating your payment. Please try requesting for your verification again.';

                SMS::sendSMS($user->phone, $sms);

                return;

            }

            $response   = json_decode($response->getBody()->getContents());
            $sms        = null;
            
            if($response->status == 'success'){

                $data               = $response->data->data;

                $sms = 'To pay for your request, Kindly dial the following ussd code: %0a';
                $sms .= $data->note.'%0a %0a';
                $sms .= 'After your successful payment, we would send you a confirmation SMS.';

            }else{

                $sms = 'We ran into some issues while initiating your payment. Please try requesting for your verification again.';
            }

            SMS::sendSMS($user->phone, $sms);

        }catch(Exception $e){

        }
    }

    public function getKey()
    {
        $secretKey                  = env('RAVE_SECRET_KEY');
        $hashedkey                  = md5($secretKey);
        $hashedkeylast12            = substr($hashedkey, -12);
        $seckeyadjusted             = str_replace("FLWSECK-", "", $secretKey);
        $seckeyadjustedfirst12      = substr($seckeyadjusted, 0, 12);

        $encryptionkey = $seckeyadjustedfirst12.$hashedkeylast12;

        return $encryptionkey;
    }

    public function encrypt3Des($data, $key)
    {
        $encData = openssl_encrypt($data, 'DES-EDE3', $key, OPENSSL_RAW_DATA);

        return base64_encode($encData);
    }
}
