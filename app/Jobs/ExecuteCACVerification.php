<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteCACVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $cac        = $this->verification->cac;
        $rc_number  = $this->verification->rc_number;

        $response = OneGov::get('cac/verify?company='.$cac.'&rcNo='.$rc_number);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'CAC '.$this->verification->bvn.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your CAC Number '.$this->verification->bvn.' is valid. %0a';
            $sms .= 'Name: '.optional($info)->Name.' %0a';
            $sms .= 'Company: '.optional($info)->Company.' %0a';
            $sms .= 'Status: '.optional($info)->Status.' %0a';
            $sms .= 'Type: '.optional($info)->TypeofEntity.' %0a';
            $sms .= 'Activity: '.optional($info)->Activity.' %0a';
            $sms .= 'Directors: '.optional($info)->Directors.' %0a';
            $sms .= 'RegistrationNo: '.optional($info)->RegistrationNo.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);       
    }
}
