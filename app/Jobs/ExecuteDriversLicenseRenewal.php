<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\DriversLicenseRenewal;
use App\Services\SMS;

class ExecuteDriversLicenseRenewal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $renewal;

    public function __construct(DriversLicenseRenewal $renewal)
    {
        $this->renewal = $renewal;
    }

    public function handle()
    {
        $this->renewal->update([

            'status'    => 'processed'
        ]);

        $sms = 'Your Drivers License Renewal was successful.';

        SMS::sendSMS($this->renewal->user->phone, $sms);
    }
}
