<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteNINVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $response = OneGov::get('nin/verify?nin='.$this->verification->nin);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'NIN '.$this->verification->nin.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your NIN Number '.$this->verification->nin.' is valid. %0a';
            $sms .= 'BirthDate: '.optional($info)->birthdate.' %0a';
            $sms .= 'BirthLGA: '.optional($info)->birthlga.' %0a';
            $sms .= 'BirthState: '.optional($info)->birthstate.' %0a';
            $sms .= 'CentralID: '.optional($info)->centralID.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);         
    }
}
