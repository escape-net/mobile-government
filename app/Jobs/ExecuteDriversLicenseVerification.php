<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteDriversLicenseVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $drivers_license    = $this->verification->drivers_license;
        $first_name         = $this->verification->first_name;
        $last_name          = $this->verification->last_name;
        $dob                = date('Y-m-d', strtotime($this->verification->birth_date));

        $response = OneGov::get('license/verify?id='.$drivers_license.'&firstname='.$first_name.'&lastname='.$last_name.'&dob='.$dob);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'Drivers License '.$this->verification->drivers_license.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your Driver\'s License '.$this->verification->bvn.' is valid. %0a';
            $sms .= 'FullName: '.optional($info)->FullName.' %0a';
            $sms .= 'DateOfBirth: '.optional($info)->DateOfBirth.' %0a';
            $sms .= 'Address: '.optional($info)->Address.' %0a';
            $sms .= 'State Of Issuance: '.optional($info)->StateOfIssuance.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);     
    }
}
