<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteBVNVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $response = OneGov::get('bvn/verify?bvn='.$this->verification->bvn);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'BVN '.$this->verification->bvn.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your BVN Number '.$this->verification->bvn.' is valid. %0a';
            $sms .= 'Name: '.optional($info)->FirstName.' '.optional($info)->MiddleName.' '.optional($info)->LastName.' %0a';
            $sms .= 'Date of Birth: '.optional($info)->DateOfBirth.' %0a';
            $sms .= 'Mobile Number: '.optional($info)->PhoneNumber1.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);    
    }
}
