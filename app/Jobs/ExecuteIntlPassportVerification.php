<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\OneGov;
use App\Verification;
use App\Services\SMS;

class ExecuteIntlPassportVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $verification;

    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    public function handle()
    {
        $intl_passport      = $this->verification->intl_passport;
        $first_name         = $this->verification->first_name;
        $last_name          = $this->verification->last_name;
        $dob                = date('Y-m-d', strtotime($this->verification->birth_date));

        $response = OneGov::get('passport/verify?passportId='.$intl_passport.'&firstname='.$first_name.'&lastname='.$last_name.'&dob='.$dob);

        $this->verification->update([

            'status'    => 'processed',
            'response'  => json_encode($response)
        ]);

        if($response->status != true || optional(optional($response->data)->ResponseInfo)->ResponseCode != '00'){

            $this->verification->update([

                'status'    => 'failed'
            ]);

            $sms = 'Intl Passport '.$this->verification->intl_passport.' is invalid';

        }else{

            $info = (object)$response->data->ResponseData;

            $sms = 'Your BVN Number '.$this->verification->bvn.' is valid. %0a';
            $sms .= 'Name: '.optional($info)->firstName.' '.optional($info)->lastName.' '.optional($info)->otherName.' %0a';
            $sms .= 'Issued At: '.optional($info)->issuedAt.' %0a';
            $sms .= 'Expiry Date: '.optional($info)->expiryDate.' %0a';
            $sms .= 'ID Number: '.optional($info)->idNumber.' %0a';
        }
        
        SMS::sendSMS($this->verification->user->phone, $sms);        
    }
}
