<?php 

namespace App\Steps;
use App\Traits\UssdHelpers;
use App\Verification;
use Carbon\Carbon;
use App\Bank;
use App\Transaction;
use Illuminate\Support\Str;
use App\Jobs\RaveTransaction;

class VerifyCAC {

    use UssdHelpers;

    public $user;
    public $data;

    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    public function index()
    {
        Verification::where([

            'user_id'   => $this->user->id, 
            'type'      => 'cac',
            'status'    => 'incomplete'

        ])->update(['status' => 'cancelled']);

        $response  = "Verify CAC \n";
        $response .= "Enter Company Name \n";

        return $this->reply($response);
    }

    public function cac()
    {
        $cac = $this->data->last();

        $verification = Verification::create([

            'user_id'   => $this->user->id,
            'type'      => 'cac',
            'cac'       => $cac,
            'amount'    => 150
        ]);

        $response  = "Verify CAC \n";
        $response .= "Please Enter RC Number \n";

        return $this->reply($response);
    }

    public function rc_number()
    {
        $rc_number  = $this->data->last();

        $verification = Verification::where([

            'user_id'   => $this->user->id, 
            'type'      => 'cac', 
            'status'    => 'incomplete'

        ])->first();

        if(!$verification)
            return $this->reply('An Error Occured please try again.', 'END');

        $verification->update(['rc_number' => $rc_number]);

        $response  = "Verify Intl Passport \n";
        $response .= "This service costs N{$verification->amount} \n";
        $response .= "1. Continue \n";
        $response .= "2. Cancel \n";

        return $this->reply($response);
    }

    public function bank()
    {
        $response   = (int)$this->data->last();

        if($response != 1)
            return $this->reply('Thank you for using this service.', 'END');

        $verification = Verification::where([

            'user_id'   => $this->user->id, 
            'type'      => 'cac', 
            'status'    => 'incomplete'

        ])->first();

        if($verification)
            $verification->update(['status' => 'pending']);
        
        $response  = "Verify CAC \n";
        $response .= "Please select your Bank \n";

        foreach(Bank::allowedBanks() as $row)
            $response .= "$row->short_code. $row->short_name \n";

        return $this->reply($response);
    }

    public function sms()
    {
        $bank = Bank::whereShortCode($this->data->last())->first();

        if(!$bank)
            return $this->reply('Please select one of the options.', 'END');

        $verification = Verification::where([

            'user_id'   => $this->user->id, 
            'type'      => 'cac', 
            'status'    => 'pending'

        ])->first();

        if(!$verification)
            return $this->reply('An Error occured.', 'END');

        $transaction = Transaction::create([

            'user_id'               => $this->user->id,
            'bank_id'               => $bank->id,
            'payment_reference'     => Str::random(10),
            'amount'                => $verification->amount,
            'status'                => 'pending'
        ]);

        $verification->update([

            'transaction_id' => $transaction->id,
            'bank_id'        => $bank->id
        ]);

        RaveTransaction::dispatch($transaction);

        $response = "You will get an SMS on how to pay shortly. \n Thank you.";

        return $this->reply($response, 'END');
    }
    
}