<?php 

namespace App\Steps;
use App\Traits\UssdHelpers;

class Registration {

	use UssdHelpers;

	public $user;
	public $data;

	public function __construct($user, $data)
	{
		$this->user = $user;
		$this->data = $data;
	}

	public function index()
	{
		$response  = "Setup Your Profile \n";
		$response .= "Enter Firstname then Surname \n";

		return $this->reply($response);
	}

	public function name()
	{
		$names = collect(explode(' ', $this->data->last()));

		$this->user->update([

			'first_name'	=> $names->get(0, ''),
			'last_name'		=> $names->get(1, '')
		]);

		$response  = "Setup Your Profile \n";
		$response .= "Enter Your Email \n";

		return $this->reply($response);
	}

	public function email()
	{
		$this->user->update([

			'email'	=> $this->data->last()
		]);

		$response  = "Your Profile has been created successfully. \n";

		return $this->reply($response, 'END');
	}
}