<?php 

namespace App\Steps;
use App\Traits\UssdHelpers;

class Initialize {

	use UssdHelpers;

	public function index()
	{
		$response  = "1Gov.ng Mobile \n";
		$response .= "1. Register \n";
		$response .= "2. BVN \n";
		$response .= "3. NIN \n";
		$response .= "4. Driver's License \n";
		$response .= "5. Int'l Passport \n";
		$response .= "6. Voter's Card \n";
		$response .= "7. CAC \n";
		$response .= "8. TIN \n";
		//$response .= "#. Next \n";

		return $this->reply($response);
	}
}