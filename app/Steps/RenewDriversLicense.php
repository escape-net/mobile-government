<?php 

namespace App\Steps;
use App\Traits\UssdHelpers;
use App\DriversLicenseRenewal;
use Carbon\Carbon;
use App\Bank;
use App\Transaction;
use Illuminate\Support\Str;
use App\Jobs\RaveTransaction;

class RenewDriversLicense {

	use UssdHelpers;

	public $user;
	public $data;

	public function __construct($user, $data)
	{
		$this->user = $user;
		$this->data = $data;
	}

	public function index()
	{
		DriversLicenseRenewal::where([

			'user_id' 	=> $this->user->id, 
			'status' 	=> 'incomplete'

		])->update(['status' => 'cancelled']);

		$response  = "Renew Driver's License \n";
		$response .= "Enter your Old License Number \n";

		return $this->reply($response);
	}

	public function birth_day()
	{
		DriversLicenseRenewal::create([

			'user_id'			=> $this->user->id,
			'license_number'	=> $this->data->last()
		]);

		$response  = "Renew Driver's License \n";
		$response .= "Enter your BirthDate D-M-Y \n";

		return $this->reply($response);
	}

	public function years()
	{
		$date = date('Y-m-d', strtotime($this->data->last()));

		if(!$date)
			return $this->reply('Please provide a valid birthday in DD-MM-YYY', 'END');

		DriversLicenseRenewal::where(['user_id'	=> $this->user->id, 'status' => 'incomplete'])->update([

			'birth_date' => $date
		]);

		$response  = "Renew Driver's License \n";
		$response .= "Please select renewal years \n";
		$response .= "1. 3years renewal \n";
		$response .= "2. 5years renewal \n";

		return $this->reply($response);
	}

	public function payment()
	{

		$years 		= 3;
		$amount		= 5250;

		$response 	= (int)$this->data->last();

		if($response == 2){

			$years 	= 5;
			$amount = 12500;
		}

		DriversLicenseRenewal::where(['user_id'	=> $this->user->id, 'status' => 'incomplete'])->update([

			'years' 	=> $years,
			'amount'	=> $amount
		]);

		$response  = "Renew Driver's License \n";
		$response .= "This service costs $amount \n";
		$response .= "1. Continue \n";
		$response .= "2. Cancel \n";

		return $this->reply($response);
	}

	public function bank()
	{
		
		$response 	= (int)$this->data->last();

		if($response != 1)
			return $this->reply('Thank you for using this service.', 'END');

		DriversLicenseRenewal::where(['user_id'	=> $this->user->id, 'status' => 'incomplete'])->update([

			'status' => 'pending'
		]);

		$response  = "Renew Driver's License \n";
		$response .= "Please select your Bank \n";

		foreach(Bank::allowedBanks() as $row)
			$response .= "$row->id. $row->short_name \n";

		return $this->reply($response);
	}

	public function sms()
	{
		$bank = Bank::find($this->data->last());

		if(!$bank)
			return $this->reply('Please select one of the options.', 'END');

		$renewal = $this->user->drivers_license_renewals()->whereStatus('pending')->first();

		if(!$renewal)
			return $this->reply('An Error occured.', 'END');

		$transaction = Transaction::create([

			'user_id'               => $this->user->id,
			'bank_id'               => $bank->id,
			'payment_reference'     => Str::random(10),
			'amount'                => $renewal->amount,
			'status'                => 'pending'
		]);

		$renewal->update([

			'transaction_id' => $transaction->id,
			'bank_id'		 => $bank->id
		]);

		RaveTransaction::dispatch($transaction);

		$response = "You will get an SMS on how to pay shortly. \n Thank you.";

		return $this->reply($response, 'END');
	}
	
}