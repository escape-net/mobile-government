<?php 

namespace App\Steps;
use App\Traits\UssdHelpers;
use App\Verification;
use Carbon\Carbon;
use App\Bank;
use App\Transaction;
use Illuminate\Support\Str;
use App\Jobs\RaveTransaction;

class VerifyBVN {

	use UssdHelpers;

	public $user;
	public $data;

	public function __construct($user, $data)
	{
		$this->user = $user;
		$this->data = $data;
	}

	public function index()
	{
		Verification::where([

			'user_id' 	=> $this->user->id, 
			'type'		=> 'bvn',
			'status' 	=> 'incomplete'

		])->update(['status' => 'cancelled']);

		$response  = "Verify BVN \n";
		$response .= "Enter a BVN Number \n";

		return $this->reply($response);
	}

	public function bvn()
	{
		$bvn = (int)$this->data->last();

		$verification = Verification::create([

			'user_id'	=> $this->user->id,
			'type'		=> 'bvn',
			'bvn'		=> $bvn,
			'amount'	=> 20
		]);

		$response  = "Verify BVN \n";
		$response .= "This service costs N{$verification->amount} \n";
		$response .= "1. Continue \n";
		$response .= "2. Cancel \n";

		return $this->reply($response);
	}

	public function bank()
	{
		$response 	= (int)$this->data->last();

		if($response != 1)
			return $this->reply('Thank you for using this service.', 'END');

		$verification = Verification::where([

			'user_id'	=> $this->user->id, 
			'type' 		=> 'bvn', 
			'status' 	=> 'incomplete'

		])->first();

		if($verification)
			$verification->update(['status' => 'pending']);
		
		$response  = "Verify BVN \n";
		$response .= "Please select your Bank \n";

		foreach(Bank::allowedBanks() as $row)
			$response .= "$row->short_code. $row->short_name \n";

		return $this->reply($response);
	}

	public function sms()
	{
		$bank = Bank::whereShortCode($this->data->last())->first();

		if(!$bank)
			return $this->reply('Please select one of the options.', 'END');

		$verification = Verification::where([

			'user_id'	=> $this->user->id, 
			'type' 		=> 'bvn', 
			'status' 	=> 'pending'

		])->first();

		if(!$verification)
			return $this->reply('An Error occured.', 'END');

		$transaction = Transaction::create([

			'user_id'               => $this->user->id,
			'bank_id'               => $bank->id,
			'payment_reference'     => Str::random(10),
			'amount'                => $verification->amount,
			'status'                => 'pending'
		]);

		$verification->update([

			'transaction_id' => $transaction->id,
			'bank_id'		 => $bank->id
		]);

		RaveTransaction::dispatch($transaction);

		$response = "You will get an SMS on how to pay shortly. \n Thank you.";

		return $this->reply($response, 'END');
	}
	
}