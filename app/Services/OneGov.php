<?php 

namespace App\Services;

use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;
use \GuzzleHttp\Exception\ClientException;
use \GuzzleHttp\Exception\ConnectException;
use \GuzzleHttp\Exception\ServerException;

class OneGov {

	public static function get($endpoint = '')
	{
		try{

			$baseUrl = env('ONEGOV_ENDPOINT', 'http://1gov-app.azurewebsites.net/api/v2');

            $response = (new Client)->get("{$baseUrl}/{$endpoint}", [

                'headers' => [

                    'Accept' => 'application/json',
                    'token'  => 'G9EOM9XX'
                ]
            ]);

            if((int)$response->getStatusCode() != 200)
                return (object)['status' => false, 'data' => $response->getBody()->getContents()];

            $response = json_decode($response->getBody()->getContents());
            $data     = $response;

            return (object)['status' => $response->status ?? true, 'data' => $data];

        }catch(Exception $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ClientException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ConnectException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
            
        }catch(ServerException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
        }
	}

    public static function post($endpoint = '', $payload = [])
    {
        try{

            $baseUrl = env('ONEGOV_ENDPOINT', 'http://1gov-app.azurewebsites.net/api/v2');

            $response = (new Client)->post("{$baseUrl}/$endpoint", [

                'headers' => [

                    'Accept' => 'application/json',
                    'token'  => 'G9EOM9XX'
                ],

                RequestOptions::JSON => $payload
            ]);

            if((int)$response->getStatusCode() != 200)
                return (object)['status' => false, 'data' => $response->getBody()->getContents()];

            $response   = json_decode($response->getBody()->getContents());
            $data       = $response;

            return (object)['status' => true, 'data' => $data];

        }catch(Exception $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ClientException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];

        }catch(ConnectException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
            
        }catch(ServerException $e){

            return (object)['status' => false, 'data' => $e->getMessage()];
        }
    }

    public static function mockResponse($type = 'bvn')
    {
        $bvn = [

            'ResponseInfo' => [

                'ResponseCode' => '00',
                'Parameter'    => '',
                'Source'       => 'BVN',
                'Message'      => 'Results Found',
                'Timestamp'    => now()->format('Y-m-d H:i:s')
            ],

            'ResponseData' => [

                'Firstname'     => 'John',
                'Lastname'      => 'Doe',
                'DOB'           => '12-May-99',
                'Mobile'        => '08175020329',
                'ImageBase64'   => ''
            ]
        ];

        if($type == 'bvn')
            return $bvn;

        return [];
    }
}