@extends('emails.layout')

@section('body')

<style type="text/css">
	.table {
		padding: 15px;
		text-align: left !important;
		border-bottom: 1px solid #ddd;
	}

	.table tr:nth-child(even) {
		background-color: #f2f2f2;
	}
</style>
<table class="table">
	@foreach($data as $key => $value)
	<tr class="table">
		<th class="table" style="text-align:left !important">{{strtoupper($key)}}</th>
		<td class="table" style="text-align:left !important">{{$value}}</td>
	</tr>
	@endforeach
</table>
@endsection